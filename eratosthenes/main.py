def create_sieve(left: int, right: int):
    return [number for number in range(left, right) if number > 1 and (number == 2 or number % 2 == 1)]


def get_primes(sieve: list):
    filtered_sieve = [{'number': number, 'deleted': False} for number in sieve]

    for i in range(int(pow(len(filtered_sieve), 0.5))):
        for j in range(i + 1, len(filtered_sieve)):
            if filtered_sieve[j]['number'] % filtered_sieve[i]['number'] == 0:
                filtered_sieve[j]['deleted'] = True

    return filtered_sieve


def main():
    sieve = create_sieve(
        int(input('Введите левый предел: ')),
        int(input('Введите правый предел: '))
    )
    filtered_sieve = get_primes(sieve)
    print(f"Простые числа: {' '.join([str(element['number']) for element in filtered_sieve if not element['deleted']])}")


if __name__ == "__main__":
    main()
