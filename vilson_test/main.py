from math import factorial


def is_primal(n: int):
    if n < 2:
        raise ValueError('Необходимо число больше 1')
    return int(factorial(n - 1) + 1) % n == 0


def main():
    n = int(input('Введите число: '))
    print(f'Число {n} {"простое" if is_primal(n) else "составное"}')


if __name__ == '__main__':
    main()
