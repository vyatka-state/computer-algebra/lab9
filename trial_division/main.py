import math


def is_primal(n: int):
    if n < 2:
        raise ValueError('Необходимо число больше 1')
    for i in range(2, int(math.sqrt(n))):
        if n % i == 0:
            return False
    return True


def main():
    n = int(input('Введите число: '))
    print(f'Число {n} {"простое" if is_primal(n) else "составное"}')


if __name__ == '__main__':
    main()
