from lab9.eratosthenes.main import create_sieve, get_primes


def a_exists(n: int, primes: list, iter_limit=100000):
    a = 1
    while a <= iter_limit and (not pow(a, n - 1, n) == 1 or any(pow(a, int((n - 1) / prime), n) == 1 for prime in primes)):
        a += 1

    if a == iter_limit + 1:
        return False
    return True


def get_all_primes(n: int):
    return [element['number'] for element in get_primes(create_sieve(2, n // 2)) if not element['deleted'] and (n - 1) % element['number'] == 0]


def is_primal(n: int):
    if n < 2:
        raise ValueError('Необходимо число больше 1')
    return a_exists(n, get_all_primes(n))


def main():
    n = int(input('Введите число: '))
    print(f'Число {n} {"простое" if is_primal(n) else "составное"}')


if __name__ == '__main__':
    main()
